﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
	public int time = 0;
	public Text timer;

	// Use this for initialization
	void Start()
	{
		StartCoroutine("Time");
	}

	// Update is called once per frame
	void Update()
	{
		timer.text = (time.ToString());

		if (time < 0)
		{
			StopCoroutine("LoseTime");
			timer.text = "Times Up!";
		}
	}

	IEnumerator Time()
	{
		while (true)
		{
			yield return new WaitForSeconds(1);
			time++;
		}
	}
}