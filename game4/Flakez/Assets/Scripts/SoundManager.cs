﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour {

	public Slider Volume;
	public AudioSource MusicSource;
	public static SoundManager instance = null;

	// Use this for initialization
	void Awake () {
		if (instance == null) {
			instance = this;
		} else if (instance != null) {
			Destroy (this.gameObject);
		}

	}
	
	// Update is called once per frame
	void Update () {
		if (Volume != null) {
			MusicSource.volume = Volume.value;
		}
	}
}
