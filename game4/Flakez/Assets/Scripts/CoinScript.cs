﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D col){ 
		ScoreScript.coinAmount += 10;
		HealthBarScript.health -= 10f;
		Destroy (gameObject);
	}
}
