﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BorderBreakScript : MonoBehaviour {

	public static float health = 100f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnCollisionEnter2D(Collision2D col){
		if (this.gameObject != null) {
			if (col.gameObject.tag.Equals ("Bullet")) {
				Destroy (GameObject.FindWithTag ("Bullet"));
			}
		}

		if (health <= 0f) {
			Destroy (gameObject);
		} else {
			health = health - 20f;
		}
	}
}
