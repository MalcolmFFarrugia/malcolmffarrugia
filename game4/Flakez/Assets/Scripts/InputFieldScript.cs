﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputFieldScript : MonoBehaviour {

	public InputField Field;
	public Text result;
	string UserName;


	// Use this for initialization
	void Start () {
		UserName = PlayerPrefs.GetString ("Username");
		result.text = UserName + " you have reached the end of the game";
	}
	
	// Update is called once per frame
	void Update () {
		string Name = Field.text;
		PlayerPrefs.SetString ("Username", Name);
	}
}
