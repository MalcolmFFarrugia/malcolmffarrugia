﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerator : MonoBehaviour {

	public GameObject thePlatform;
	public GameObject theCoin;

	public Transform generationPoint;

	public float distanceBetween;

	public float distanceBetweenMin;
	public float distanceBetweenMax;

	private float minHeight;
	private float maxHeight;

	public Transform maxHeightPoint;
	public float maxHeightChange;
	private float heightChange;

	private float platformWidth;

	// Use this for initialization
	void Start () {
		platformWidth = thePlatform.GetComponent<BoxCollider2D> ().size.x;

		minHeight = transform.position.y;
		maxHeight = maxHeightPoint.position.y;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (transform.position.x < generationPoint.position.x) {
			distanceBetween = Random.Range (distanceBetweenMin, distanceBetweenMax);
			heightChange = transform.position.y + Random.Range (maxHeightChange, -maxHeightChange);

			if (heightChange > maxHeight) {
				heightChange = maxHeight;
			} else if (heightChange < minHeight) {
				heightChange = minHeight;
			}
				
			PlatformUpdate ();
		}
	}

	void PlatformUpdate(){
		transform.position = new Vector3 (transform.position.x + platformWidth + distanceBetween, heightChange, transform.position.z);
		Instantiate (thePlatform, transform.position, transform.rotation);

		if (thePlatform == true) {
			transform.position = new Vector3 (transform.position.x, heightChange + 2f, transform.position.z);
			Instantiate (theCoin, transform.position, transform.rotation);
		}
	}
		
}
