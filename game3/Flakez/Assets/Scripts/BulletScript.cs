﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

	public float speed = 5f;
	public GameObject Bullet;
	public AudioSource Sound;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Fire1"))
		{
			Shoot();
			Sound.Play ();
		}

	}

	void Shoot(){
		Vector3 worldMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		Vector2 direction = (Vector2)((worldMousePos - transform.position));
		direction.Normalize();

		// Creates the bullet locally
		GameObject FirePointGame = (GameObject)Instantiate(Bullet,transform.position + (Vector3)(direction * 1f),Quaternion.identity);

		// Adds velocity to the bullet
		FirePointGame.GetComponent<Rigidbody2D>().velocity = direction * speed;
	}

	void OnTriggerEnter2D(Collider2D col){ 
		Destroy (this.gameObject);
	}
		
}