﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class playerMovement : MonoBehaviour {

	//float playerPos;
	public float speed = 5f;
	public AudioSource Background;
	private bool Sound = true; 



	// Use this for initialization
	void Start () {
		//playerPos = transform.position.y;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		//jump
		if(Input.GetKey(KeyCode.Space)){
			GetComponent<Rigidbody2D> ().velocity = new Vector3 (0, 5f, 0);
		}

		//walk right
		if(Input.GetKey(KeyCode.D)){
			transform.Translate (Vector3.right * speed * Time.deltaTime);
		}

		//walk left
		if (Input.GetKey (KeyCode.A)) {
			transform.Translate (Vector3.left * speed * Time.deltaTime);
		}

		//suicide
		if (Input.GetKey (KeyCode.Backspace)) {
			Scene scene = SceneManager.GetActiveScene(); 
			SceneManager.LoadScene(scene.name);
		}

		//running
		if (Input.GetKey (KeyCode.LeftShift)) {
			speed = 15f;
		} else {
			speed = 5f;
		}

		if(Input.GetKeyDown(KeyCode.M)){
			if (Sound == true) {
				Background.Pause ();
				Sound = false;
			} else {
				Background.Play ();
				Sound = true;
			}
		}
			
	}
}
