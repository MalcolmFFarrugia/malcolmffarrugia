﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class levelManager : MonoBehaviour {

	// Use this for initialization
	public void play()
	{
		SceneManager.LoadScene(1);
	}

	public void info()
	{
		SceneManager.LoadScene(2);
	}

	public void back(){
		SceneManager.LoadScene (0);
	}

	public void quit()
	{
		Application.Quit ();
	}
}
