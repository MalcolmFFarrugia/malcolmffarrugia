﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BorderBreakScript : MonoBehaviour {

	public static float health = 100f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnCollisionEnter2D(Collision2D col){ 
		Destroy (col.gameObject);
		if (health <= 0f) {
			Destroy (gameObject);
		} else {
			health = health - 20f;
		}
	}
}
