﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputFieldScript : MonoBehaviour {

	public InputField Field;
	static string UserName;


	// Use this for initialization
	void Start () {
		UserName = PlayerPrefs.GetString ("UserName");
		Field.text = UserName;
	}
	
	// Update is called once per frame
	void Update () {

	}
}
